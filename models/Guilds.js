module.exports = (sequelize, DataTypes) => {
    return sequelize.define('guilds', {
        guild_id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        mod_channel_id: {
            type: DataTypes.STRING,
            defaultValue: "",
            allowNull: false,
        },
    }, {
        timestamps: false,
    });
};