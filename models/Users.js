module.exports = (sequelize, DataTypes) => {
    return sequelize.define('users', {
        user_id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        coins: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
        },
        latest_daily: {
            type: DataTypes.DATE,
            defaultValue: 0,
            allowNull: true,
        }
    }, {
        timestamps: false,
    });
};