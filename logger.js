const pino = require('pino') // Import pino

const logger = pino({ // Create logger
    transport: {
      target: 'pino-pretty' // Set that we format
    },
})

module.exports = logger // Export logger
