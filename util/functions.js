const { Op } = require('sequelize');
const { Users } = require("../dbObjects.js");

module.exports = client => {
    client.addXp = async (id, xp) => {
        const user = await Users.findByPk(id); // We find the user

        if (user) { // If it exists, we add the xp and stop there
            user.xp += Number(xp);
            return user.save()
        }

        const newUser = await Users.create({ user_id: id, balance: xp, level: Math.floor(0.1 * Math.sqrt(xp)) }); // We create the user with the xp and the good level

        return newUser;
    }

    client.getXp = async (id) => { // Used to get the xp, 0 if the user doens't exist
        const user = await Users.findByPk(id);
        return user ? user.xp : 0;
    }

    client.updateLevel = async (id, level) => { // Used to set the level
        const user = await Users.findByPk(id)

        user.level = level
        return user.save()
    }

    client.getLevel = async (id) => { // Used to get the level, 0 if the user doens't exist
        const user = await Users.findByPk(id);
        return user ? user.level : 0;
    }

    client.removeXp = async (id, xp) => {
        const user = await Users.findByPk(id); // We find the user

        if (user) { // If it exists, we add the xp and stop there
            user.xp -= Number(xp);
            return user.save()
        }

        const newUser = await Users.create({ user_id: id, balance: xp, level: Math.floor(0.1 * Math.sqrt(xp)) }); // We create the user with the xp and the good level

        return newUser;
    }

    client.getTop10 = async (id) => { // Get global top 10. 
        const top10 = await Users.findAll({
            order: [["xp", "DESC"]],
            limit: 10
        });
        return top10;
    }

    // TODO: getTop10 but on the current guild. The main use case is single guild but the bot support sharding.

}