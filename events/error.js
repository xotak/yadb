const { Events } = require('discord.js');
const logger = require("../logger")

module.exports = {
    name: Events.Error,
    async execute(m) {
        logger.error(m)
    }
}