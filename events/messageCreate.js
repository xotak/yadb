const { Events } = require('discord.js');

module.exports = {
    name: Events.MessageCreate,
    async execute(message) {
        if (message.author.bot) return; // Don't do anything if message is from a bot

        const authorId = message.author.id // We get the id of author

        const expCd = Math.floor(Math.random() * 19) + 1; // Generate a random number
        
        if (expCd >= 8 && expCd <= 12 ) { // if xpCd is in range, we add Xp. It kinda works as a cooldown
            const expToAdd = Math.floor(Math.random() * 40) + 15  // Generate amount of xp
            await message.client.addXp(authorId, message.guild.id, expToAdd);
        }

        const currentXp = await message.client.getXp(authorId, message.guild.id)

        // New level calculation
        const userLevel = Math.floor(50*(currentXp**2) + 25 * currentXp) // Here's the level curve. The same as Arcane
        if (await message.client.getLevel(authorId, message.guild.id) < userLevel) { // If current user level higher is lower than the one we calculated
            message.reply(`You just reached level ${userLevel}! GG!`) // We announce level up
            await message.client.updateLevel(authorId, message.guild.id, userLevel) // And we edit the database
        }
    },
};
