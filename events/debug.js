const { Events } = require('discord.js');
const logger = require("../logger")

module.exports = {
    name: Events.Debug,
    async execute(m) {
        logger.debug(m)
    }
}