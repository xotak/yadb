const { Events } = require('discord.js');
const logger = require("../logger")

module.exports = {
    name: Events.Warn,
    async execute(m) {
        logger.warn(m)
    }
}