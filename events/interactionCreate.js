const { Events } = require('discord.js');
const logger = require("../logger")

module.exports = {
    name: Events.InteractionCreate,
    async execute(interaction) {
        if (interaction.isChatInputCommand()) {

            const command = interaction.client.commands.get(interaction.commandName); // We retrive the executed command from the collection

            if (!command) {
                logger.error(`No command matching ${interaction.commandName} found`); // We log an error if the command is not found. A case I'm thinking of when this error would trigger is when the command file gets deleted, but the commands aren't registered again
            }

            try {
                await command.execute(interaction); // We execute the command
            } catch (error) {
                logger.error(error); // We log the error
                if (interaction.replied || interaction.deferred) {  // If we already replied to the command, or is waiting for an reply, we do a follow up
                    await interaction.followUp({ content: 'There was an error while executing this command!', ephemeral: true });
                } else { // Else we just do a normal reply
                    await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
                }
            }
        }
    },
};
