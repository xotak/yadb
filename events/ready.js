const { Events } = require('discord.js');
const logger = require("../logger")

    module.exports = {
        name: Events.ClientReady,
        once: true,
        async execute(client) {
            if (typeof client.shard.ids !== 'undefined' && client.shard.ids !== null) {
            logger.info(`Shard ${client.shard.ids} ready! Logged in as ${client.user.tag}`); // Some logging telling the shard is ready and as who is he connected.
        } else {
            logger.warn("UNSHARDED")
            logger.info(`Ready! Logged in as ${client.user.tag}`)
        }
        client.moon.init(client.user.id)
    },
};
