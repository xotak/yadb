// Bunch of libs
const { Events, AttachmentBuilder } = require('discord.js');
const Canvas = require('@napi-rs/canvas');
const { request } = require('undici')

// applyText calculates a fontsize from the size of the canvas
const applyText = (canvas, text) => {
    const context = canvas.getContext('2d');
    let fontSize = 70;

    do {
        context.font = `${fontSize -= 10}px sans-serif`;
    } while (context.measureText(text).width > canvas.width - 300);

    return context.font;
};

module.exports = {
    name: Events.GuildMemberAdd,
    async execute(member) {
        // Creating the canvas
        const canvas = Canvas.createCanvas(700, 250)
        const context = canvas.getContext('2d')

        // Draw the background
        const background = await Canvas.loadImage('./util/background.png')
        context.drawImage(background, 0, 0, canvas.width, canvas.height)

        // Write welcome
        context.font = '28px sans-serif';
        context.fillStyle = '#ffffff';
        context.fillText('Welcome', canvas.width / 2.5, canvas.height / 3.5);

        // Select the font size and type from one of the natively available fonts
        context.font = applyText(canvas, `${member.displayName}`);

        // Select the style that will be used to fill the text in
        context.fillStyle = '#ffffff';

        // Actually fill the text with a solid color
        context.fillText(member.displayName, canvas.width / 2.5, canvas.height / 1.8);

        // Write member count
        context.font = '28px sans-serif';
        context.fillStyle = '#ffffff';
        context.fillText(`Member #${member.guild.memberCount}`, canvas.width / 2.5, canvas.height / 1.4);

        // Pick up the pen
        context.beginPath();

        // Start the arc to form a circle
        context.arc(125, 125, 100, 0, Math.PI * 2, true);

        // Put the pen down
        context.closePath();

        // Clip off the region you drew on
        context.clip();

        // Using undici to make HTTP requests for better performance
        const { body } = await request(member.displayAvatarURL({ extension: 'jpg' }));
        const avatar = await Canvas.loadImage(await body.arrayBuffer());

        // Draw a shape onto the main canvas
        context.drawImage(avatar, 25, 25, 200, 200);

        // Create attachement
        const attachement = new AttachmentBuilder(await canvas.encode('png'), { name: 'profile-image.png' })

        // Send attachment in server's system channel
        member.guild.systemChannel.send({ files: [attachement] })
    }
}