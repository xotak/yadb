const logger = require("../logger")

module.exports = {
    name: "nodeCreate",
    async execute(node) {
        logger.info(`${node.host} was connected. Lavalink ready`);
    }
}