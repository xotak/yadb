const logger = require("../logger")

module.exports = {
    name: "warn",
    async execute(m) {
        logger.warn(m)
    }
}