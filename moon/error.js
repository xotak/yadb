const logger = require("../logger")

module.exports = {
    name: "error",
    async execute(m) {
        logger.error(m)
    }
}