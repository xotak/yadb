const { SlashCommandBuilder } = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("stop")
        .setDescription("Clears the queue and disconnect"),
    async execute(interaction) {
        await interaction.deferReply()
        if (!interaction.member.voice.channel) {
            // Responding with a message if the user is not in a voice channel
            return interaction.followUp({
                content: `You are not in a voice channel`,
                ephemeral: true
            });
        } else if (!interaction.client.moon.players.get(interaction.guildId)) {
            // Responding with a message if no player is found
            return interaction.followUp({
                content: `No player found`,
                ephemeral: true
            });
        }

        let player = interaction.client.moon.players.get(interaction.guildId)

        await player.stop()
        await player.destroy()

        await interaction.followUp("Queue cleared and disconected. Bye :wave:")

    }
}