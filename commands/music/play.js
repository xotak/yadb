const { SlashCommandBuilder, EmbedBuilder } = require("discord.js");
const ms = require("ms")

module.exports = {
    data: new SlashCommandBuilder()
        .setName("play")
        .setDescription("Play a song or a playlist")
        .addStringOption(option =>
            option.setName("query")
                .setDescription("The song you're seraching")
                .setRequired(true)
        ),
    async execute(interaction) {
        await interaction.deferReply()
        if (!interaction.member.voice.channel) {
            // Responding with a message if the user is not in a voice channel
            return interaction.followUp({
                content: `You are not in a voice channel`,
                ephemeral: true
            });
        }

        let query = interaction.options.getString("query");
        let player = interaction.client.moon.players.create({
            guildId: interaction.guild.id,
            voiceChannelId: interaction.member.voice.channel.id,
            textChannelId: interaction.channel.id,
            autoLeave: true,
            autoPlay: false,
            loop: 0,
            shuffle: false
        });

        if (!player.connected) {
            // Connecting to the voice channel if not already connected
            player.connect({
                setDeaf: true,
                setMute: false
            });
        }

        let res = await interaction.client.moon.search({
            query,
            source: "youtube",
            requester: interaction.user.id
        });

        if (res.loadType === "loadfailed") {
            await player.stop()
            await player.destroy()
            // Responding with an error message if loading fails
            return interaction.followUp({
                content: `:x: Load failed - the system is not cooperating.`
            });
        } else if (res.loadType === "empty") {
            await player.stop()
            await player.destroy()
            // Responding with a message if the search returns no results
            return interaction.followUp({
                content: `:x: No matches found!`
            });
        }


        const embed = new EmbedBuilder()
            .setColor("Purple")
            .setTitle("Adding Music.")
            .setImage(res.tracks[0].artworkUrl)
            .setFooter({
                text: `Requested by: ${interaction.member.displayName}`,
            });

        if (res.loadType === "playlist") {
            embed.setTitle(`Adding Playlist.`);
            embed.addFields(
              {
                name: `*\`Playlist Name:\`*`,
                value: res.playlistInfo.name,
                inline: true,
              },
              {
                name: `*\`Duration:\`*`,
                value: `${ms(res.playlistInfo.duration, { long: true})}`,
                inline: true,
              },
            );

            for (const track of res.tracks) {
                // Adding tracks to the queue if it's a playlist
                player.queue.add(track);
            }

        } else {
            embed.addFields(
              {
                name: `*\`Title:\`*`,
                value: `[${res.tracks[0].title}](${res.tracks[0].url})`,
                inline: true,
              },
              {
                name: `*\`Uri:\`*`,
                value: `${res.tracks[0].url}`,
                inline: true,
              },
              {
                name: `*\`Author:\`*`,
                value: `${res.tracks[0].author}`,
                inline: true,
              },
              {
                name: `*\`Duration:\`*`,
                value: `${ms(Number(res.tracks[0].duration), {long: true})}`,
                inline: true,
              },
            );
                        
            player.queue.add(res.tracks[0]);
        }

        await interaction.followUp({embeds : [embed]})

        if (!player.playing) {
            // Starting playback if not already playing
            player.play();
        }
    }
}