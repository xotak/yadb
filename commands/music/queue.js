const { SlashCommandBuilder, EmbedBuilder } = require("discord.js")
const ms = require('ms')

module.exports = {
    data: new SlashCommandBuilder()
        .setName("queue")
        .setDescription("Sends the current queue")
        .setDMPermission(false),
    async execute(interaction) {
        await interaction.deferReply()
        if (!interaction.member.voice.channel) {
            // Responding with a message if the user is not in a voice channel
            return interaction.followUp({
                content: `You are not in a voice channel`,
                ephemeral: true
            });
        } else if (!interaction.client.moon.players.get(interaction.guildId)) {
            // Responding with a message if no player is found
            return interaction.followUp({
                content: `No player found`,
                ephemeral: true
            });
        }

        const player = await interaction.client.moon.players.get(interaction.guildId)

        const queue = player.queue.all

        const embed = new EmbedBuilder()
            .setAuthor({ name: `${interaction.client.user.displayName}`, iconURL: `${interaction.client.user.avatarURL()}`})
            .setTitle("Next 10 track in queue")
            .setFooter({ text: `Requested by ${interaction.user.displayName}`, iconURL: `${interaction.user.avatarURL()}`})
            .setColor("Purple")


        for (let i = 0; i < 10; i++) {
            const track = queue[i]
            const title = track.title
            const author = track.author
            const url = track.url
            const duration = ms(track.duration, { long: true })

            embed.addFields(
                {name: `Track ${i+1}`, value: `${author} -  [${title}](${url}) (${duration})`}
            )
        }

        await interaction.followUp({ embeds: [embed] })
    }
}