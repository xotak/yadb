const { SlashCommandBuilder } = require("discord.js")

module.exports = {
    data: new SlashCommandBuilder()
        .setName("loop")
        .setDescription("Loops the current title")
        .addStringOption(option =>
            option.setName("mode")
            .setDescription("The loop mode")
            .addChoices(
                { name: "Track", value: "track"},
                { name: "Queue", value: "queue"},
                { name: "Off", value: "off"}
            )
            .setRequired(true)),
    async execute(interaction) {

        const mode = await interaction.options.getString("mode")

        if (!interaction.member.voice.channel) {
            // Responding with a message if the user is not in a voice channel
            return interaction.followUp({
                content: `You are not in a voice channel`,
                ephemeral: true
            });
        } else if (!interaction.client.moon.players.get(interaction.guildId)) {
            // Responding with a message if no player is found
            return interaction.followUp({
                content: `No player found`,
                ephemeral: true
            });
        }

        const player = await interaction.client.moon.players.get(interaction.guildId)

        player.setLoop(mode)

        if (mode === "track") {
            return interaction.reply("Track loop enabled !")
        } else if (mode === "queue") {
            return interaction.reply("Queue loop enabled !")
        } else {
            interaction.reply("Looping turned off ")
        }
    }
}