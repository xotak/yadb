const { SlashCommandBuilder, EmbedBuilder } = require("discord.js")
const ms = require('ms')

module.exports = {
    data: new SlashCommandBuilder()
        .setName("nowplaying")
        .setDescription("Sends the currently playing song")
        .setDMPermission(false),
    async execute(interaction) {
        await interaction.deferReply()
        if (!interaction.member.voice.channel) {
            // Responding with a message if the user is not in a voice channel
            return interaction.followUp({
                content: `You are not in a voice channel`,
                ephemeral: true
            });
        } else if (!interaction.client.moon.players.get(interaction.guildId)) {
            // Responding with a message if no player is found
            return interaction.followUp({
                content: `No player found`,
                ephemeral: true
            });
        }

        const player = await interaction.client.moon.players.get(interaction.guildId)

        const track = player.current

        const embed = new EmbedBuilder()
            .setColor("Purple")
            .setTitle("Currently playing : ")
            .setImage(track.artworkUrl)
            .setFooter({
                text: `Requested by: ${interaction.member.displayName}`,
            })
            .addFields(
                {
                  name: `*\`Title:\`*`,
                  value: `[${track.title}](${track.url})`,
                  inline: true,
                },
                {
                  name: `*\`Uri:\`*`,
                  value: `${track.url}`,
                  inline: true,
                },
                {
                  name: `*\`Author:\`*`,
                  value: `${track.author}`,
                  inline: true,
                },
                {
                  name: `*\`Duration:\`*`,
                  value: `${ms(Number(track.duration), {long: true})}`,
                  inline: true,
                },
              );

        await interaction.followUp({embeds: [embed]})

    }
}