const { SlashCommandBuilder } = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("addexp")
        .setDescription("Add some experience to the selected user")
        .addUserOption(option => 
            option.setName("user")
                .setDescription("The user to execute the action on")
                .setRequired(true)
        )
        .addIntegerOption(option => 
            option.setName("xptoadd")
                .setDescription("The amount of xp to add")
                .setRequired(true)
        ),

    async execute(interaction) {
        const user = await interaction.options.getUser("user")
        const xp = await interaction.options.getInteger("xptoadd")

        await interaction.client.addXp(user.id, interaction.guild.id, xp);

        await interaction.reply(`Gave ${user.displayName} ${xp} xp.`)

        // New level calculation
        const userLevel = Math.floor(0.1 * Math.sqrt(await interaction.client.getXp(user.id, interaction.guild.id))) // A level is 0.1 times the square root of the total user xp
        if (await interaction.client.getLevel(user.id, interaction.guild.id) != userLevel) { // If current user level higher is lower than the one we calculated
            interaction.followUp(`${user.displayName} just reached level ${userLevel}! GG!`) // We announce level up
            await interaction.client.updateLevel(user.id, interaction.guild.id, userLevel) // And we edit the database
        }
    },
}
