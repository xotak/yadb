const { SlashCommandBuilder, EmbedBuilder } = require("discord.js")

module.exports = {
    data: new SlashCommandBuilder()
        .setName("leaderboard")
        .setDescription("Sends the XP learderboard of the server")
        .setDMPermission(false),
    async execute(interaction) {
        await interaction.deferReply()

        const users = await interaction.client.getTop10XP(interaction.guild.id)

        const embed = new EmbedBuilder()
            .setAuthor({ name: `${interaction.client.user.displayName}`, iconURL: `${interaction.client.user.avatarURL()}`})
            .setTitle("Leaderboard")
            .setFooter({ text: `Requested by ${interaction.user.displayName}`, iconURL: `${interaction.user.avatarURL()}`})
            .setColor("Purple")

        for (const user of users) {
            const data = user.dataValues
            const userId = data.user_id
            const xp = data.xp
            const level = data.level

            const userSnowflake = await interaction.guild.members.fetch(userId)

            embed.addFields(
                {name: `${userSnowflake.displayName}`, value: `Experience points : ${xp}, Level : ${level}`}
            )
        }

        await interaction.followUp({ embeds: [embed] })
    }
}