const { SlashCommandBuilder } = require("discord.js");
const logger = require('../../logger')

module.exports = {
    data: new SlashCommandBuilder()
        .setName("killshard")
        .setDescription("Kill the specified shard")
        .addIntegerOption(option =>
            option.setName('shardid')
            .setDescription('The ID of the shard to kill')
            .setRequired(true)),
    async execute(interaction) {
        const app = await interaction.client.application.fetch()
        if (interaction.user.id != app.owner.id) return;
        
        const shardID = interaction.options.getInteger("shardid")

        const params = {
            shard: shardID,
        }
        
        await interaction.reply({ content: `Killing shard ${shardID}`, ephemeral: true})

        interaction.client.shard.broadcastEval( (c, {shard}) => {
            if (c.shard.ids.includes(shard)) {
                process.exit();
            }
        }, { context: params });
        logger.warn(`Shard ${shardID} killed`)
    }
}