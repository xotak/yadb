const { SlashCommandBuilder } = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName("restart")
        .setDescription("Assuming sharded, restarts all the shards"),

    async execute(interaction) {
        const app = await interaction.client.application.fetch()
        if (interaction.user.id != app.owner.id) return;
        await interaction.reply({ content: 'Restarting shards, please wait', ephemeral: true});
        await interaction.client.shard.respawnAll();
    }
}