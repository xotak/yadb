const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const ms = require('ms');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("botinfo")
        .setDescription("Provides informations about the bot"),
    async execute(interaction) {
        await interaction.deferReply()
        
        const guildCacheSize = await interaction.client.shard.fetchClientValues('guilds.cache.size')
        const serverCount = await guildCacheSize.reduce((acc, guildCount) => acc + guildCount, 0)

        const guildsMemberCount = await interaction.client.shard.broadcastEval(c => c.guilds.cache.reduce((acc, guild) => acc + guild.memberCount, 0))
        const memberCount = await guildsMemberCount.reduce((acc, memberCount) => acc + memberCount, 0)

        const application = await interaction.client.application.fetch()
        
        const embed = new EmbedBuilder()
            .setColor('Purple')
            .setAuthor({ name: `${interaction.client.user.displayName}`, iconURL: `${interaction.client.user.avatarURL()}`})
            .setTitle("About me")
            .setDescription("Here's some infos about the bot")
            .addFields(
                {name: "Total servers", value: `${serverCount}`},
                {name: "Total members", value: `${memberCount}`},
                {name: "Current shard", value: `${interaction.client.shard.ids}`},
                {name: "Total shards", value: `${interaction.client.shard.count}`},
                {name: "Shard uptime", value: `${ms(interaction.client.uptime)}`}
            )
            .setFooter({ text: `Made by ${application.owner.displayName} (@${application.owner.tag})`, iconURL: `${application.owner.avatarURL()}` });
        await interaction.followUp({ embeds: [embed] })
    }
}