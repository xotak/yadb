const { SlashCommandBuilder, EmbedBuilder, hyperlink, hideLinkEmbed } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("help")
        .setDescription("Prints the help message"),
    async execute(interaction) {
        await interaction.deferReply()

        const application = await interaction.client.application.fetch()
        
        const embed = new EmbedBuilder()
            .setColor('Purple')
            .setAuthor({ name: `${interaction.client.user.displayName}`, iconURL: `${interaction.client.user.avatarURL()}`})
            .setTitle("Get help")
            .setDescription("Here's where you can get help for the bot")
            .addFields(
                {name: "Documentation", value: `${hyperlink("Available here", "https://xotak.frama.io/yadb-docs/")}`},
                {name: "Source code", value: `${hyperlink("Available here", "https://framagit.org/xotak/yadb")}`},
            )
            .setFooter({ text: `Made by ${application.owner.displayName} (@${application.owner.tag})`, iconURL: `${application.owner.avatarURL()}` });
        await interaction.followUp({ embeds: [embed] })
    }
}