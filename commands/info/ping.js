const { SlashCommandBuilder } = require("discord.js");
const { EmbedBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName("ping")
        .setDescription("Replies with stats about bot latency!"),

    async execute(interaction) {
        const wsPing = interaction.client.ws.ping
        const sent = await interaction.reply({ content: 'Pinging...', fetchReply: true });
        const roundtripPing = sent.createdTimestamp - interaction.createdTimestamp

        const embed = new EmbedBuilder()
            .setColor("Orange")
            .setAuthor({ name: `${interaction.client.user.displayName}`, iconURL: `${interaction.client.user.avatarURL()}`})
            .setTitle('Ping results')
            .addFields(
                {name: 'WebSocket heartbeat', value: `${wsPing}ms`},
                {name: 'Roundtrip latency', value: `${roundtripPing}ms`}
            )
            .setFooter({ text: `Requested by ${interaction.user.displayName}`, iconURL: `${interaction.user.avatarURL()}`})
        
            interaction.editReply({ embeds: [embed] })
    }
}