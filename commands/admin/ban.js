const { SlashCommandBuilder, EmbedBuilder, PermissionFlagsBits } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('ban')
        .setDescription('Bans the selected user')
        .addUserOption(option => 
            option.setName("user")
            .setDescription("The user to ban")
            .setRequired(true))
        .addStringOption(option => 
            option.setName("reason")
            .setDescription("The reason of the ban"))
        .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers)
        .setDMPermission(false),
    async execute(interaction) {
        await interaction.deferReply()

        const userToBan = await interaction.options.getMember("user")
        const reason = await (interaction.options.getString("reason") || "No reason specified")

        const embed = new EmbedBuilder()
            .setAuthor({name: `${userToBan.displayName} (${userToBan.id})`})
            .setColor('Red')
            .setDescription(`**Action**: ban\n**Reason**: ${reason}`)
            .setThumbnail(userToBan.user.avatarURL())
            .setTimestamp()
            .setFooter({text: `${interaction.member.user.tag}`, iconURL: interaction.member.user.displayAvatarURL()})

        try {
            await userToBan.ban(reason);
            await interaction.followUp({ embeds: [embed] })
        } catch (error) {
            if (error instanceof DiscordAPIError && error.message === "Missing Permissions") {
                interaction.followUp("I have not enough permissions to time this user out")
            } else {
                throw error
            }
        }
    },
};
