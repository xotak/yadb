const { SlashCommandBuilder, EmbedBuilder, PermissionFlagsBits } = require('discord.js')

module.exports = {
    data: new SlashCommandBuilder()
        .setName("cleartimeout")
        .setDescription("Removes the timeout of an user")
        .addUserOption(option =>
            option.setName('user')
            .setDescription('The user that have been timed out')
            .setRequired(true))
        .setDefaultMemberPermissions(PermissionFlagsBits.ModerateMembers)
        .setDMPermission(false),
    async execute(interaction) {
        await interaction.deferReply()

        const userToClear = (await interaction.options.getMember('user'))

        const embed = new EmbedBuilder()
            .setAuthor({name: `${userToClear.displayName} (${userToClear.id})`})
            .setColor('Green')
            .setDescription(`**Action**: Timeout Removed`)
            .setThumbnail(userToClear.user.avatarURL())
            .setTimestamp()
            .setFooter({text: `${interaction.member.user.tag}`, iconURL: interaction.member.user.displayAvatarURL()})

        try {
            await userToClear.timeout(null);
            await interaction.followUp({ embeds: [embed] })
        } catch (error) {
            if (error instanceof DiscordAPIError && error.message === "Missing Permissions") {
                interaction.followUp("I have not enough permissions to time this user out")
            } else {
                throw error
            }
        }
    }
}