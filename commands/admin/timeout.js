const { SlashCommandBuilder, EmbedBuilder, DiscordAPIError, PermissionFlagsBits } = require('discord.js')
const ms = require('ms')
const logger = require("../../logger")

module.exports = {
    data: new SlashCommandBuilder()
        .setName('timeout')
        .setDescription('Times the selected user out')
        .addUserOption(option =>
            option.setName('user')
            .setDescription('The user to timeout')
            .setRequired(true))
        .addIntegerOption(option => 
            option.setName('duration')
            .setDescription('The duration of the timeout in minutes'))
        .addStringOption(option => 
            option.setName('reason')
            .setDescription('Reason of the timeout'))
        .setDefaultMemberPermissions()
        .setDMPermission(false),
    async execute(interaction) {
        await interaction.deferReply(PermissionFlagsBits.ModerateMembers);

        const userToTimeout = await interaction.options.getMember('user')
        const reason = (await interaction.options.getString('reason') ?? "No reason specified")

        const durationOption = await interaction.options.getInteger('duration') // The duration from option
        var timeoutDuration

        timeoutDuration = (ms(`${durationOption}m`) ?? ms('10m'))

        const embed = new EmbedBuilder()
            .setAuthor({name: `${userToTimeout.displayName} (${userToTimeout.id})`})
            .setColor('Red')
            .setDescription(`**Action**: timeout\n**Reason**: ${reason}\n**Duration**: ${ms(timeoutDuration, {long: true})}`)
            .setThumbnail(userToTimeout.user.avatarURL())
            .setTimestamp()
            .setFooter({text: `${interaction.member.user.tag}`, iconURL: interaction.member.user.displayAvatarURL()})

        try {
            await userToTimeout.timeout(timeoutDuration, reason);
            await interaction.followUp({ embeds: [embed] })
        } catch (error) {
            if (error instanceof DiscordAPIError && error.message === "Missing Permissions") {
                interaction.followUp("I have not enough permissions to time this user out")
            } else {
                throw error
            }
        }
        
    }
}