const { SlashCommandBuilder } = require('@discordjs/builders')
const { Client } = require("undici")
const client = require('nekos.life')

module.exports = {
    data: new SlashCommandBuilder()
        .setName("owoify")
        .setDescription("OwOifies the text you give")
        .addStringOption(option =>
            option.setName("string")
            .setDescription("The text you want to OwOify")
            .setRequired(true)
        ),
    async execute(interaction) {
        await interaction.deferReply()

        const neko = new client()

        const owo = await neko.OwOify({text: await interaction.options.getString("string")}) 

        await interaction.followUp(owo.owo)

    }
}