const { SlashCommandBuilder } = require('@discordjs/builders')
const { Client } = require("undici")
const client = require('nekos.life')

module.exports = {
    data: new SlashCommandBuilder()
        .setName("nekos")
        .setDescription("Neko pictures (=;ェ;=)"),
    async execute(interaction) {
        await interaction.deferReply()

        const neko = new client()

        const picture = await neko.neko() 

        await interaction.followUp(picture.url)

    }
}