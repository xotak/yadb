const { SlashCommandBuilder } = require('@discordjs/builders')
const { Client } = require("undici")

module.exports = {
    data: new SlashCommandBuilder()
        .setName("waifu")
        .setDescription("Shows a waifu picture")
        .addBooleanOption(option =>
            option.setName("nsfw")
                .setDescription("Allow nsfw search results (only works in nsfw channels)")
        ),
    async execute(interaction) {
        await interaction.deferReply()

        const client = new Client('https://api.waifu.im')

        switch (await interaction.options.getBoolean("nsfw") ?? false) {
            case true:
                const isChannelNsfw = await interaction.channel.nsfw

                if (!isChannelNsfw) {
                    await interaction.followUp("Caught you being horny outside of an nsfw channel huh")
                } else {
                    const { statusCode, body } = await client.request({
                        path: '/search?included_tags=waifu&is_nsfw=true',
                        method: 'GET',
                    })
                    // Check if the response is ok
                    if (statusCode === 200) {
                        // Read the response body as JSON
                        const data = await body.json();
                        const image = data.images[0]
                        
                        await interaction.followUp(image.url)


                    } else {
                        await interaction.followUp("Something went wrong with waifu.im");
                    }
                }
                break
            case false:
                const { statusCode, body } = await client.request({
                    path: '/search?included_tags=waifu&is_nsfw=false',
                    method: 'GET',
                })
                // Check if the response is ok
                if (statusCode === 200) {
                    // Read the response body as JSON
                    const data = await body.json();
                    const image = data.images[0]

                    await interaction.followUp(image.url)

                } else {
                    await interaction.followUp("Something went wrong with waifu.im");
                }
        }

    }
}