# YADB - Yet Another Discord Bot

Features list : 
- Slash commands
- Ping
- Moderation
- Leveling
- Music (from Youtube)

(more soon...)

## Installation

First step is to download the bot's code. Use git or download the zip.

Then, you'll will need node.js and npm to run the bot. Install that.

Then in the folder containing the bot's code, open up a terminal and run `npm i`.

## Configuration

Copy the default-config.json located in the util folder in the main folder. Fill up the config file with apropriate values.

## Running the bot

On the main folder, run `npm run deployCommands` to send the commands to Discord's server and run `npm run start` to launch the bot.

Hopefully automated install script soon (Windows + Linux)

## TODOs

### Economy

- [ ] Migrate XP to another database (Member: guildId, userId, level, xp)  
- [ ] Economy : add little coin each messages  
- [ ] Commands : /daily and bunch of other stuff  
    

## Thanks

- Thanks to [1Lucas1apk](https://github.com/1Lucas1apk) for his assistance with moonlink