const Sequelize = require('sequelize');
const logger = require('./logger.js');

const sequelize = new Sequelize('database', 'username', 'password', { // Create connection to database
    host: 'localhost',
    dialect: 'sqlite',
    logging: false,
    storage: 'database.sqlite',
});

require('./models/Users.js')(sequelize, Sequelize.DataTypes); // Import the model
require('./models/Members.js')(sequelize, Sequelize.DataTypes); // Import the model
require('./models/Guilds.js')(sequelize, Sequelize.DataTypes); // Import the model


const force = process.argv.includes('--force') || process.argv.includes('-f'); // check if -f or --force is passed. Used to overwrite an existing database

sequelize.sync({ force }).then(async () => {
    logger.info('Database synced'); // Writing database
    sequelize.close(); // Closing connection
}).catch(logger.error); // We catch and log the error of something goes wrong
