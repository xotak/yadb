// Import libraries
const { ShardingManager } = require("discord.js");
const { token } = require('./config.json');

// Create the logger and the shard manager
const logger = require('./logger')
const manager = new ShardingManager('./bot.js', {token: token})

// Declare event : when shard is created, log that it have been connected
manager.on('shardCreate', shard => logger.info(`Launched shard ${shard.id}`))

// Spawn manager
manager.spawn()