const Sequelize = require('sequelize'); 


// Creating connection to SQLite database
const sequelize = new Sequelize('database', 'username', 'password', { 
    host: 'localhost',
    dialect: 'sqlite',
    logging: false,
    storage: 'database.sqlite',
});

// Import the model ...
const Users = require('./models/Users.js')(sequelize, Sequelize.DataTypes);
const Members = require('./models/Members.js')(sequelize, Sequelize.DataTypes);
const Guilds = require('./models/Guilds.js')(sequelize, Sequelize.DataTypes);


module.exports = { Users, Members, Guilds } // ... and export it as an object