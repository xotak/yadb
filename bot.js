// Import filesystem, discord.js and token
const fs = require('node:fs');
const path = require('node:path');
const { Client, Collection, Events, GatewayIntentBits } = require("discord.js");
const { token } = require('./config.json');
const logger = require('./logger.js');
const { Manager } = require("moonlink.js")

// Create the client
const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.GuildMembers, GatewayIntentBits.GuildVoiceStates] });
require('./util/functions.js')(client)
require('./util/assetsFetcher.js')(client)

/* ===================
Dynamic command import
==================== */ 

// Create commands collection
client.commands = new Collection();

// Get the path of the commands dir 
const foldersPath = path.join(__dirname, 'commands');
const commandFolders = fs.readdirSync(foldersPath);

// For each sub-folder of the commands folder
for (const folder of commandFolders) {
    // Get the path of the sub-folder
    const commandsPath = path.join(foldersPath, folder);
    const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
    // For each file of the subfolder
    for (const file of commandFiles) {
        // Get the path of the file
        const filePath = path.join(commandsPath, file);
        // Import the command
        const command = require(filePath);
        // Set a new item in the Collection with the key as the command name and the value as the exported module
        if ('data' in command && 'execute' in command) {
            client.commands.set(command.data.name, command);
        } else {
            logger.warn(`The command at ${filePath} is missing a required "data" or "execute" property.`);
        }
    }
}

/* ==================
 Dynamic event import 
================== */


// Get the path of the events folder
const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));

// For each event file
for (const file of eventFiles) {
    // Get the path of the file
    const filePath = path.join(eventsPath, file);
    // Import the event
    const event = require(filePath);
    // Execution
    if (event.once) {
        client.once(event.name, (...args) => event.execute(...args));
    } else {
        client.on(event.name, (...args) => event.execute(...args));
    }
}

/* ======
 Moonlink
====== */

client.moon = new Manager({
    nodes: [{
        identifier: "local_node",
        host: "localhost",
        port: 2333,
        secure: false,
        password: "youshallnotpass"
    }],
    options: {},
    sendPayload: (guildId, payload) => {
        client.guilds.cache.get(guildId).shard.send(JSON.parse(payload));
    }
});

// Get the path of the events folder
const moonEventsPath = path.join(__dirname, 'moon');
const moonEventFiles = fs.readdirSync(moonEventsPath).filter(file => file.endsWith('.js'));

// For each event file
for (const file of moonEventFiles) {
    // Get the path of the file
    const filePath = path.join(moonEventsPath, file);
    // Import the event
    const event = require(filePath);
    // Execution
    if (event.once) {
        client.moon.once(event.name, (...args) => event.execute(...args), client);
    } else {
        client.moon.on(event.name, (...args) => event.execute(...args), client);
    }
}

// Event: Raw data
client.on("raw", data => {
    // Updating the Moonlink.js package with the necessary data
    client.moon.packetUpdate(data);
});

// Log in the bot
client.login(token);